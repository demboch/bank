package bank;

import java.util.ArrayList;
import java.util.List;

public class History {

	private List<HistoryLog> _logsList;
	
    public History(){
        _logsList = new ArrayList<HistoryLog>();
    }
    
    public void log(HistoryLog log){
        _logsList.add(log);
    }
    
    public String toString() 
    {	
		String details = "Historia: " + _logsList;
		return details.toString();
	}
}
