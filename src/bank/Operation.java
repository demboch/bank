package bank;

public abstract class Operation {

	private String _name;
	
		public abstract void execute(Account account);

		public String getName() {
			return _name;
		}

		public void setName(String name) {
			this._name = name;
		}
}
