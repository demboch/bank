package bank;

public class Account {

	private String _num;
    private History _history;
    private double _amount;
    
    public Account(String num){
        _num = num;
        _history = new History();
        _amount = 0;
    }
    public Account(String num, double amount){
        _num = num;
        _amount = amount;
        _history = new History();
    }   
    
    public void add(double amount){
        _amount += amount; 
        HistoryLog hl = new HistoryLog("Kwota: " + amount, OperationType.INCOME);
        _history.log(hl);   
    }
    
    public void substract(double amount){
        _amount -= amount;
        HistoryLog hl = new HistoryLog("Kwota: " + amount, OperationType.OUTCOME);
        _history.log(hl);    
    }
    
    public void doOperation(Operation op){
        op.execute(this);  
    }
 
    public String getNumber() {
    	return this._num;
    }
    	    
    public double getAmount() {
    	return this._amount;
    }
    
	public History get_history() {
		return _history;
	}
    
//	public String historyLogs() {
//		String details = "Logi: ";
//		for (HistoryLog log : this._history.toString()) {
//			details += log.toString();
//		}
//		return details.toString();
//	}
}
