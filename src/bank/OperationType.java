package bank;

public enum OperationType {

	INCOME("Przych�d"), OUTCOME("Transfer");
	
	private String description;
	
	OperationType(String operation) {
		this.description = operation;
	}
	
	public String getOperationDesc() {
		return this.description;
	}
}
