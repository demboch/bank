package bank;

public class Bank {

	private Operation income;
	private Operation transfer;
    
    public void income(Account account, double amount){ 
        income = new Income(account);
        account.doOperation(income);
    }
    
    public void transfer(Account accountFrom, Account accountTo, double amount){
    	transfer = new Transfer(accountTo, amount);
    	accountFrom.doOperation(transfer); 
    }
}
