package bank;

import java.util.Date;

public class HistoryLog {

	private Date _date;
    private String _title;
    private OperationType _opType;
     
    public HistoryLog(String title, OperationType _opType) {
    	_date = new Date();
		this._title = title;
		this._opType = _opType;
	}
    
    public String logs()
    {
    	String details = "Date: " + _date.toLocaleString();
    	details += "Title: " + _title;
    	details += "Operation Type: " + _opType;
    	
		return details.toString();
    } 
}
