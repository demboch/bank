package bank;

public class Main {

	public static void main(String[] args) {
		
		Bank bank = new Bank();
		
		Account account1 = new Account("1351 3522 3563 3563 2345 8766");
		Account account2 = new Account("7655 1234 8763 2367 5672 3742");
		Account account3 = new Account("3578 6492 1571 3579 2572 1456");
		
		System.out.println("Witamy w Banku!!!");
		
		bank.income(account1, 300);
		bank.income(account2, 200);
		bank.income(account3, 250);
		
		System.out.println("Konto 1: " + account1.getAmount() + " z�., nr konta: " + account1.getNumber());
		System.out.println("Konto 2: " + account2.getAmount() + " z�., nr konta: " + account2.getNumber());
		System.out.println("Konto 3: " + account3.getAmount() + " z�., nr konta: " + account3.getNumber());
		
		System.out.println("\nKonto 1 przesy�a 100 z� na Konto 2.\n");
		
		bank.transfer(account1, account2, 100);
		
		System.out.println("Konto 1 posiada: " + account1.getAmount() + " z�.");
		System.out.println("Konto 2 posiada: " + account2.getAmount() + " z�.");
		System.out.println("Konto 3 posiada: " + account3.getAmount() + " z�.");
		
		System.out.println("\nHistoria konto 1\n");
		System.out.println(account1.get_history());
		System.out.println("\nHistoria konto 2\n");
		System.out.println(account2.get_history());
		System.out.println("\nHistoria konto 2\n");
		System.out.println(account3.get_history());

		// kilka b�ed�w kt�rychnie zdazylem poprawic 
	}

}
