package bank;

public class Transfer extends Operation{

	private Account from, to;
	private double amount;
	
    public Transfer(Account to, double amount) {
    	this.setName("Transfer");
        this.to = to;
        this.amount = amount;
    }
    
	@Override
	public void execute(Account account) 
	{
		this.from = account;
		this.from.substract(amount);
        this.to.add(amount);
	}
}
