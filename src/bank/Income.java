package bank;

public class Income extends Operation {

	private Account account;
	private double amount;
	
    public Income(Account account) {
		this.setName("Wplata");
        this.account = account;
    }

    @Override
    public void execute(Account account) {
        this.account = account;
        this.account.add(amount);
    }
}
